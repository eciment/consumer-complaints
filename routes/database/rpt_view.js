var sqlite3 = require('sqlite3').verbose();
var db;

exports.get = function(req, res){
	console.log("Serving " + __filename);
	
	var db_name = 'cfpb';
	var table = req.params.table;
	
	var dbpath = 'databases/' + db_name;
	db = new sqlite3.Database(dbpath, function(err){
		if (err) res.render('database/db_edit', {message: 'Error: ' + err});	
	});
	
	//Get summary data
	var getSumData = 'SELECT Count("Complaint ID") as countOfComplaints, avg(B06010_001) as avgPop, min(b06011_001) as minMedianInc, max(b06011_001) as maxMedianInc, avg(B06012_001) as avgPoverty FROM blend;';
	
	//Gets the total number of complaints registered in the db
	var getComplaintsCount = 'SELECT COUNT ("Complaint ID") as countOfComplaints FROM complaints;';

	//Gets all complaints that have valid ZIP codes attached to them
	var getValidZipComplaintsCount = 'SELECT Count("Complaint ID") FROM blend;';	

	var message, complaintCountMsg, avgPopulationMsg, minMedianIncomeMsg, maxMedianIncomeMsg, avgPovertyLevelMsg;
	var countOfComplaints, zipComplaints, avgPopulation, minMedianIncome, maxMedianIncome, avgPovertyLevel;
	message = "Consumer Complaints Data Blended with ACS Data"

	var dataRows;
   

	db.all(getSumData, function(err, rows) {
    	    
    	    dataRows = rows;
    	    console.log(rows[0]);
    	
    });

    db.all(getComplaintsCount, function(err, rows) {
    	    
    	if (rows.length > 0)
    	{
	        countOfComplaints = rows[0].countOfComplaints;	       
    	}
    


    	res.render('database/rpt', {message: message, countOfComplaints: countOfComplaints, dataRows: dataRows});

    	db.close();
    });
	
        
};